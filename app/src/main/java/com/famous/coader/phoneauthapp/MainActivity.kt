package com.famous.coader.phoneauthapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    // variable for Auth
    private lateinit var auth: FirebaseAuth
    // variable for our text input
    // field for phone and OTP
    private lateinit var phoneEditText: EditText
    private lateinit var otpEditText: EditText

    // buttons for generating OTP and verifying OTP
    private lateinit var sendOTP: AppCompatButton
    private lateinit var verifyOtpButton: AppCompatButton

    private var storeVerificationId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        auth = FirebaseAuth.getInstance()

        phoneEditText = findViewById(R.id.phone_editText)
        otpEditText = findViewById(R.id.otp_editText)

        sendOTP= findViewById(R.id.sendOtp_appCompatButton)
        verifyOtpButton = findViewById(R.id.verifyOtp_appCompatButton)

        sendOTP.setOnClickListener {
            val phone = "+91" + phoneEditText.text.toString()
            val phoneAuth = PhoneAuthOptions.newBuilder(auth)
                .setPhoneNumber(phone)
                .setTimeout(60L, TimeUnit.SECONDS)
                .setActivity(this)
                .setCallbacks( object : PhoneAuthProvider.OnVerificationStateChangedCallbacks(){
                    override fun onCodeSent(verificationId: String, p1: PhoneAuthProvider.ForceResendingToken) {
//                        super.onCodeSent(p0, p1)
                        Toast.makeText(this@MainActivity, "OTP is sent", Toast.LENGTH_SHORT).show()

                        storeVerificationId = verificationId
                    }
                    override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                    }

                    override fun onVerificationFailed(p0: FirebaseException) {
                    }

                })
                .build()

            PhoneAuthProvider.verifyPhoneNumber(phoneAuth)
        }

        verifyOtpButton.setOnClickListener {
            val otpText = otpEditText.text.toString()
            val credential =PhoneAuthProvider.getCredential(storeVerificationId, otpText )

            auth.signInWithCredential(credential)
                .addOnSuccessListener {
                    Toast.makeText(this, "Login or verify success", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, HomeActivity::class.java))
                }
                .addOnFailureListener {
                    Toast.makeText(this, "Login failed", Toast.LENGTH_SHORT).show()

                }

        }
    }
}